# Ad Exchanger

<p>
Ad Exchanger is a core of the OpenRTB platform.<br>
It receives and processes requests from websites and other networks.

This should be a high-load distributed application, running instances may have different<br>
public IP addresses, but the last idea may change in future.

On startup it caches some data from the DB and tries to connect to the cache manager,<br>
but can work without it. When cache manager has new data or some updated data,<br>
it sends them in JSON format to all instances of Ad Exchanger.

On shutdown it sends a signal to the cache server that this particular instance stops.
</p>