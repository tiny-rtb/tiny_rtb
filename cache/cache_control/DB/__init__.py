import logging
import asyncpg as pg

from configparser import ConfigParser
from typing import Union, List, Any


class Database:
    """

    """

    @staticmethod
    async def create_conn(conf: ConfigParser) -> Union[pg.Connection, None]:
        """creates connection to the Database"""
        try:
            conn = await pg.connect(user=conf.get('postgres', 'user'), password=conf.get('postgres', 'pass'),
                                    database=conf.get('postgres', 'database'),
                                    host=conf.get('postgres', 'host'), port=conf.get('postgres', 'port'))
            return conn
        except Exception as e:
            logging.critical(str(e))

    # -------- Cache functions --------
    @staticmethod
    async def cache_ad_units(conn: pg.Connection) -> Union[None, List]:
        """fetches values for cache."""
        query = "SELECT * FROM tiny_exchange.public.ad_units"
        try:
            if res := await conn.fetch(query):
                return res
        except Exception as e:
            logging.error(e)

    @staticmethod
    async def cache_providers(conn: pg.Connection) -> Union[None, List]:
        """fetches values for cache."""
        query = "SELECT * FROM tiny_exchange.public.ad_providers"
        try:
            if res := await conn.fetch(query):
                return res
        except Exception as e:
            logging.error(e)
    # ---------------------------------
