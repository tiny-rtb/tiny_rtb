import pytest

from ad_exchanger.DB import Database
from ad_exchanger.config import config
from ad_exchanger.RTB import Request
from asyncpg import Connection
from uuid import uuid4


@pytest.mark.asyncio
@pytest.fixture
async def get_conn() -> Connection:
    return await Database.create_conn(config)


@pytest.mark.asyncio
async def test_conn(get_conn):
    """
    test for DB connection and data fetching
    """

    conn = get_conn
    assert conn is not None
    res = await conn.fetch('SELECT 1 as "test" FROM tiny_exchange.public.users LIMIT 1')
    assert res[0]['test'] == 1, "response must be 1"
    await conn.close()
    assert conn.is_closed()


@pytest.mark.asyncio
async def test_store_request_info_without_bids(get_conn):
    conn: Connection = get_conn
    assert conn is not None

    # 1 - create Request instance
    req = Request()
    req_resp_id = str(uuid4())
    req.bid_request = {'id': req_resp_id, 'imp': [{'id': '1', 'video': {'w': 640, 'h': 480, 'startdelay': 0, 'linearity': 1, 'api': [1, 2]}}], 'at': 2, 'test': 1, 'app': {'bundle': 'com.app.test'}, 'device': {'ua': 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Mobile/15E148 Safari/604.1', 'ip': '65.66.66.66'}}
    req.bid_response = {'id': req_resp_id, 'cur': 'USD', 'bidid': '86fc18dc-dbce-4c40-a334-fdffddd32eea', 'seatbid': []}

    # 2 - try to store it with Database.store_request_response_info
    req_id = await Database.store_request_response_info(conn=conn, request=req, ad_unit_id=1, ip='127.0.0.1')

    # 3 - try to fetch stored request from DB with returned ID
    req_check = await conn.fetchrow('SELECT * FROM public.request_response WHERE id = $1 AND ad_unit_id = $2', req_id, 1)

    # 4 - compare requests
    assert req_resp_id == req_check['req_resp_id'], "Request IDs are different"
    assert req_check['no_bids'] is True, "No bids field is not True, but must be"

    await conn.close()
    assert conn.is_closed()

