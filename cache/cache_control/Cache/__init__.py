import logging

from asyncpg.connection import Connection
from ..DB import Database
from typing import List, Union, Dict


class Cache:

    @staticmethod
    async def cache_ad_units(conn: Connection) -> Union[Dict, None]:
        try:
            ad_units = await Database.cache_ad_units(conn)
        except Exception as e:
            logging.critical("Unable to load ad units cache - %s", str(e))
            return
        else:
            logging.info("Ad units cache uploaded")

            ad_units = Cache.list_to_dict(ad_units, 'endpoint_hash')
            for unit in ad_units:  # convert values ro redis-compatible types
                ad_units[unit]['creation_date'] = str(ad_units[unit]['creation_date'])
                ad_units[unit]['is_active'] = int(ad_units[unit]['is_active'])
                ad_units[unit]['is_test'] = int(ad_units[unit]['is_test'])
            return ad_units

    @staticmethod
    async def cache_ad_providers(conn: Connection) -> Union[Dict, None]:
        try:
            providers = await Database.cache_providers(conn)
        except Exception as e:
            logging.critical("Unable lo load providers cache - %s", str(e))
            return
        else:
            logging.info("Providers cache uploaded")

            providers = Cache.list_to_dict(providers, 'id')
            for_remove = []
            for p in providers:
                if providers[p]['endpoint'] is not None:
                    # convert values ro redis-compatible types
                    providers[p]['is_active'] = int(providers[p]['is_active'])
                    providers[p]['is_test'] = int(providers[p]['is_test'])
                else:
                    for_remove += p  # gather all providers without endpoints to remove them
            for i in for_remove:
                del providers[i]

            return providers

    @staticmethod
    def list_to_dict(_list: List, key: str) -> Dict:
        """transforms list of dicts to dict of dicts"""
        _dict = dict()
        if _list:
            for d in _list:
                d = dict(d)
                k = d.pop(key)
                _dict[str(k)] = d
        return _dict
