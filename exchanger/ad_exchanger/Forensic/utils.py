from typing import Union
from multidict import CIMultiDict, CIMultiDictProxy


def get_client_ip(headers: Union[CIMultiDict, CIMultiDictProxy]) -> Union[str, None]:
    if ip := headers.get('x-forwarded-for'):
        proxies = ip.split(',')
        ip = proxies[0].strip()
    elif ip := headers.get('forwarded'):
        values = ip.split(';')
        ip_in_values = False
        for v in values:
            if 'for=' in v:
                ip = v[4:]
                ip_in_values = True
        if ip_in_values is False:
            ip = None
    return ip
