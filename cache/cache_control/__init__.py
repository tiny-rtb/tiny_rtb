import asyncio
import aioredis
import os
import logging

from aiohttp import web
from .test import Test
from .config import config
from .DB import Database
from .Cache import Cache

event_loop = asyncio.new_event_loop()
asyncio.set_event_loop(event_loop)
event_loop = asyncio.get_event_loop()

# using different Redis DBs (0 - 15) for different caches (ad units, providers, etc.)
if os.environ.get('IN_CONTAINER'):
    cache_ad_units = event_loop.run_until_complete(aioredis.create_redis_pool('redis://redis', db=0))
    cache_providers = event_loop.run_until_complete(aioredis.create_redis_pool('redis://redis', db=1))
else:
    cache_ad_units = event_loop.run_until_complete(aioredis.create_redis_pool('redis://127.0.0.1', db=0))
    cache_providers = event_loop.run_until_complete(aioredis.create_redis_pool('redis://127.0.0.1', db=1))

test_cache = Test()


async def on_startup(app_):

    # create connection and retrieve all cache values
    conn = await Database.create_conn(config)
    ad_units = await Cache.cache_ad_units(conn)
    ad_providers = await Cache.cache_ad_providers(conn)
    await conn.close()

    # pull specific cache values to specific Redis DB
    try:
        for unit in ad_units:
            await cache_ad_units.hmset_dict(unit, ad_units[unit])
    except Exception as e:
        logging.error("Unable to cache ad units to Redis - %S", e)
    else:
        logging.info("Ad units cached to Redis")

    try:
        for p in ad_providers:
            await cache_providers.hmset_dict(p, ad_providers[p])
    except Exception as e:
        logging.error("Unable to cache providers to Redis - %S", e)
    else:
        logging.info("Providers cached to Redis")


async def on_shutdown(app_):
    global cache_ad_units
    global cache_providers

    if cache_ad_units:
        cache_ad_units.close()
        await cache_ad_units.wait_closed()
    if cache_providers:
        cache_providers.close()
        await cache_providers.wait_closed()


app = web.Application()
app.on_shutdown.append(on_shutdown)
app.on_startup.append(on_startup)
app.add_routes([
    web.get('/', test_cache.get_hit_count)
])
