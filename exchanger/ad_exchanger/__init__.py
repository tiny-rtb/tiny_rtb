import os
import logging
import redis

from aiohttp import web
from typing import Tuple


# LOGGING INIT
kwargs = {
    'format': "%(levelname)s::%(asctime)s::%(pathname)s::%(funcName)s - %(message)s",
    'datefmt': "%Y-%m-%d %a %H:%M %Z",
    'level': logging.WARNING
}
if os.environ.get("IN_CONTAINER"):
    kwargs['filename'] = '/var/logs/rtb/rtb_log.log'

logging.basicConfig(**kwargs)
# ------------


def redis_init() -> Tuple[redis.Redis, redis.Redis]:
    if os.environ.get('IN_CONTAINER'):
        try:
            cache_ad_units = redis.Redis('redis', db=0)
            cache_providers = redis.Redis('redis', db=1)
        except Exception as e:
            logging.critical("Can't connect to Redis cache service - %s", str(e))
            quit()
        else:
            logging.info("Connected to Redis service")
            return cache_ad_units, cache_providers
    else:
        try:
            cache_ad_units = redis.Redis('127.0.0.1', db=0)
            cache_providers = redis.Redis('127.0.0.1', db=1)
        except Exception as e:
            logging.critical("Can't connect to Redis cache service - %s", str(e))
            quit()
        else:
            logging.info("Connected to Redis service")
            return cache_ad_units, cache_providers


async def on_startup(app_):
    logging.warning('startup')


async def hello(request):
    return web.json_response({'status': 'OK', 'DEBUG': int(os.environ.get('DEBUG', 0))})
