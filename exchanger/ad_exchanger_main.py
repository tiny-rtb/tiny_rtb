import asyncio
import logging

from aiohttp import web
from ad_exchanger import hello, on_startup, redis_init
from ad_exchanger.DB import Database
from ad_exchanger.config import config
from ad_exchanger.BidRequestHandler import BidRequestHandler


app = web.Application()
app.on_startup.append(on_startup)

event_loop = asyncio.new_event_loop()
asyncio.set_event_loop(event_loop)
event_loop = asyncio.get_event_loop()
# rename in future. create individual connection for each handler (if it is needed)
conn = event_loop.run_until_complete(Database.create_conn(config))
cache_ad_units, cache_providers = redis_init()


async def on_shutdown(app_):
    global conn

    tasks = [t for t in asyncio.all_tasks() if t is not
             asyncio.current_task()]

    for task in tasks:
        # skipping over shielded coro still does not help
        if task._coro.__name__ == "cant_stop_me":
            continue
        task.cancel()

    await asyncio.gather(*tasks, return_exceptions=True)

    await conn.close()
    logging.warning('shutdown')

app.on_shutdown.append(on_shutdown)

bid_request_handler = BidRequestHandler(conn, cache_ad_units, cache_providers)

app.add_routes([
    web.post(r'/openrtb/2/{hash}', bid_request_handler.process_request),
    web.get(r'/', hello),
])

if __name__ == '__main__':
    web.run_app(app)
