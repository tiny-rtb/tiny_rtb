import os
import asyncio
import itertools as it
import logging

from aiohttp import ClientSession, ClientTimeout, ClientConnectionError, InvalidURL
from typing import Dict, Any, Optional, List, Union
from operator import itemgetter


class Request:
    """
    This class contains methods for receiving, and sending Bid requests
    and receiving and storing Bid responses.
    """

    def __init__(self):
        self.bid_request = dict()  # for bid requests from SSPs to DSPs
        self.bid_response = dict()  # for bid responses from DSPs to SSPs

    @staticmethod
    async def send(method: str, dsp: Dict[str, str], data: Optional[Any] = None,
                   headers: Optional[Dict[str, str]] = None, session: Optional[ClientSession] = None) -> dict:
        """sends one request to one destination"""

        # checking for session
        if session is None:
            if headers:
                session = ClientSession(headers=headers)
            else:
                session = ClientSession()

        # sending request
        # ToDo - is this a good way to handle requests ?
        try:
            response = await session.request(method, dsp['endpoint'], json=data)
        except asyncio.TimeoutError:
            logging.error('%s for URL - %s', 'timeout', dsp['endpoint'])
        except ClientConnectionError as e:
            logging.error('exception - %s', str(e))
        except InvalidURL:
            logging.error('invalid URL - %s', dsp['endpoint'])
        except Exception as e:
            logging.error('some other exception - %s, type - %s', str(e), type(e))
        else:
            if response.status == 200:

                try:  # try to fetch JSON. Fails if JSON not valid or body is not a JSON
                    body = await response.json()
                except Exception as e:
                    body = {}
                    logging.error('exception - %s for URL - %s', str(e), dsp['endpoint'])
                else:
                    if 'seatbid' in body:  # process normal response
                        for seat in body['seatbid']:
                            if seat.get('ext'):
                                seat['ext']['provider_id'] = dsp['id']
                            else:
                                seat['ext'] = dict()
                                seat['ext']['provider_id'] = dsp['id']
            elif response.status == 204:
                body = {}
            elif response.status == 400:
                body = {}
                logging.critical("Bad request - %s", dsp['endpoint'])
            elif response.status == 500:
                body = {}
                logging.critical("Something with endpoint - %s", dsp['endpoint'])
            else:
                reason = response.reason
                logging.warning("Unexpected error - %s", reason)
                body = {}
            response.close()
            return body
        return {}

    async def send_dsp_requests(self, destinations: List[Dict[str, str]], headers: Dict[str, str]) -> List[Dict[str, str]]:
        """sends one Bid request to many destinations (DSPs)"""
        async with ClientSession(headers=headers, timeout=ClientTimeout(total=0.3)) as session:
            coros = [Request.send('POST', dsp, self.bid_request, session=session) for dsp in destinations
                     if dsp['endpoint'] is not None]
            results = await asyncio.gather(*coros, return_exceptions=True)
        return results


class RTB(Request):
    """
    Consists of functions for bidding decisions etc.
    Extends Request class with RTB-specific methods.
    """

    def __init__(self):
        super().__init__()

    price = itemgetter('price')

    # ToDo - rewrite this shit
    # yes, I use 0(zero) here because max price can be only one
    # but I wish to change an approach
    def first_price(self) -> None:
        """chooses win bid for each impression by first price algorithm"""
        win_bids = list()
        bids = list(self.bid_response['seatbid'])
        impressions = list(self.bid_request['imp'])
        for imp in impressions:
            max_by_seat = list()
            for seatbid in bids:
                seat_bids = [bid for bid in seatbid['bid'] if bid['impid'] == imp['id'] and bid['price'] >= imp['bidfloor']]
                if seat_bids:
                    max_by_seat.append({'seat': seatbid['seat'], 'bid': [max(seat_bids, key=RTB.price)], 'ext': seatbid['ext']})
            if max_by_seat:
                win_bids.append(max(max_by_seat, key=lambda x: x['bid'][0]['price']))
        self.bid_response['seatbid'] = RTB.group_by_seat(self.process_macro(win_bids))

    # ToDo - rewrite this shit !!!!
    def second_price(self) -> None:
        """chooses win bid for each impression by second price algorithm"""
        win_bids = list()
        bids = list(self.bid_response['seatbid'])
        impressions = list(self.bid_request['imp'])
        for imp in impressions:
            max_by_seat = list()
            for seatbid in bids:
                seat_bids = [bid for bid in seatbid['bid'] if bid['impid'] == imp['id'] and bid['price'] >= imp['bidfloor']]
                if seat_bids:
                    max_by_seat.append({'seat': seatbid['seat'], 'bid': [max(seat_bids, key=RTB.price)], 'ext': seatbid['ext']})
            if len(max_by_seat) == 1:
                win_bids.extend(max_by_seat)
            elif len(max_by_seat) > 1:
                win = max(max_by_seat, key=lambda x: x['bid'][0]['price'])
                max_by_seat.remove(win)
                prev = max(max_by_seat, key=lambda x: x['bid'][0]['price'])
                win['bid'][0]['price'] = prev['bid'][0]['price']
                win_bids.append(win)
        self.bid_response['seatbid'] = RTB.group_by_seat(self.process_macro(win_bids))

    # ToDO - is it possible to do it effectively ?
    @staticmethod
    def group_by_seat(bids: List) -> List:
        """groups plain list of bids by seat id after win bids have been chosen"""
        grouped_bids = list()
        for k, v in it.groupby(bids, key=lambda x: x['seat']):
            for i in v:
                same_item = [bid for bid in grouped_bids if bid['seat'] == k]
                if same_item:
                    same_item[0]['bid'].extend(i['bid'])
                else:
                    grouped_bids.append(i)
        return grouped_bids

    async def call_nurls(self, destinations: List[Dict[str, str]], headers: Optional[Dict[str, str]] = None) -> List:
        """receives markup by nurl. WORK IN PROGRESS"""
        async with ClientSession() as session:
            # ToDO - change this coros definition
            coros = [Request.send('POST', url, self.bid_request, headers, session=session) for url in destinations]
            results = await asyncio.gather(*coros, return_exceptions=True)
        return results

    def process_macro(self, seats: List[Dict[str, Union[str, List[Dict[str, str]]]]]) -> List:
        """substitutes macros into bid`s adm and nurl/burl/lurl fields"""
        for seat in seats:
            for bid in seat['bid']:
                for key in ['nurl', 'burl', 'lurl', 'adm']:
                    if key not in bid.keys():
                        continue

                    bid[key] = bid[key].replace('${AUCTION_ID}', self.bid_response['id']) \
                                       .replace('${AUCTION_BID_ID}', self.bid_response.get('bidid', '')) \
                                       .replace('${AUCTION_IMP_ID}', bid.get('impid', '')) \
                                       .replace('${AUCTION_SEAT_ID}', str(seat.get('seat', ''))) \
                                       .replace('${AUCTION_AD_ID}', bid.get('adid', '')) \
                                       .replace('${AUCTION_PRICE}', str(bid.get('price', ''))) \
                                       .replace('${AUCTION_CURRENCY}', str(self.bid_response.get('cur', ''))) \
                                       # .replace('${${AUCTION_MBR}}', ) \  ToDO - check if it is needed
        return seats

    # validates Bid request`s body
    @staticmethod
    def validate_bid_request(body: Dict) -> None:
        """validates incoming Bid request, rises an error if validation falls"""
        # body validation
        if 'id' not in body:
            raise Exception("Request ID not in body")
        if 'imp' not in body:
            raise Exception("No impressions")
        if 'device' not in body:
            raise Exception("No device")
        if ('site' not in body) and ('app' not in body):
            raise Exception("No site/app info")

        # impression validation
        types = ['video', 'audio', 'banner', 'native']
        if not len(body['imp']):
            raise Exception("Impressions length is zero")
        for imp in body['imp']:
            if 'id' not in imp:
                raise Exception(f"No ID for impression of {body['id']} request")
            imp_keys = [k.lower() for k in imp.keys()]
            if not sum([True if i in imp_keys else False for i in types]):
                raise Exception(f"No impression type for some impression of {body['id']} request")

    # generates body for Bid request to DSP service
    def dsp_side(self, body: Dict[str, Any]) -> None:
        """processes request info"""
        # required by OpenRTB 2.5 spec
        self.bid_request['id'] = str(body['id'])
        self.bid_request['imp'] = list(body['imp'])

        # optional by OpenRTB 2.5 spec
        if (at := body.get('at')) and (at := int(at)) in [1, 2]:
            self.bid_request['at'] = at
        else:
            self.bid_request['at'] = 2

        self.bid_request['test'] = (body.get('test', 0) or os.environ.get('DEBUG', 0))
        if 'tmax' in body:
            self.bid_request['tmax'] = int(body['tmax'])
        if 'ext' in body:
            self.bid_request['ext'] = dict(body['ext'])

        # recommended by OpenRTB 2.5 spec
        for key in ['site', 'app', 'user', 'device', 'source', 'regs']:
            if key in body:
                self.bid_request[key] = dict(body[key])
        if 'cur' in body:
            self.bid_request['cur'] = list(body['cur'])

    # generates body for Bid response to SSP service
    def ssp_side(self, body: Dict[str, Any]) -> None:
        """processes response info"""
        self.bid_response['id'] = body['id']
        self.bid_response['cur'] = body['cur']
        self.bid_response['bidid'] = body['bidid']
        self.bid_response['seatbid'] = body['seatbid']
