import os
from pathlib import Path

from configparser import ConfigParser

config = ConfigParser()
CURRENT = os.path.dirname(os.path.abspath(__file__))
if os.environ.get("DEBUG"):
    config.read(os.path.join(CURRENT, 'config_develop.ini'))
else:
    config.read(os.path.join(CURRENT, 'config_prod.ini'))
