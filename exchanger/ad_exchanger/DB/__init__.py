import json
import logging
import asyncpg as pg
import asyncio

from ad_exchanger.RTB import Request
from configparser import ConfigParser
from typing import Dict, Union, List, Any, Coroutine


class Database:
    """

    """

    @staticmethod
    async def create_conn(conf: ConfigParser) -> Union[pg.Connection, None]:
        """creates connection to the Database"""
        try:
            conn = await pg.connect(user=conf.get('postgres', 'user'), password=conf.get('postgres', 'pass'),
                                    database=conf.get('postgres', 'database'),
                                    host=conf.get('postgres', 'host'), port=conf.get('postgres', 'port'))
            return conn
        except Exception as e:
            logging.critical(str(e))

    # ---------------------- IMP/WON BIDS ---------------------- #

    @staticmethod
    async def store_impressions(conn: pg.Connection, impressions: List) -> None:
        """stores impressions with connection to specific request by request_id field"""

        query = """INSERT INTO tiny_exchange.public.impressions (request_id, id, bidfloor, ad_type, ad_size)
                   VALUES($1, $2, $3, $4, $5)"""
        try:
            await asyncio.shield(conn.executemany(query, impressions))
        except Exception as e:
            logging.error(str(e))

    @staticmethod
    async def store_won_bids(conn: pg.Connection, bids: List) -> None:
        """
        stores won bids with connection to specific request by response_id field.
        this naming was chosen because, actually, won bids related to response,
        but request and response have the same ID
        """
        query = """INSERT INTO tiny_exchange.public.bids (response_id, id, imp_id, seat_id, price, ad_provider_id)
                    VALUES ($1, $2, $3, $4, $5, $6)"""
        try:
            await asyncio.shield(conn.executemany(query, bids))
        except Exception as e:
            logging.error(str(e))
    # ---------------------- IMP/WON BIDS ---------------------- #

    @staticmethod
    async def log_request_err(conn: pg.Connection, ad_unit_id: Union[int, None], err: str, request: Union[Dict, None],
                              ip: str) -> None:

        json_body = json.dumps(request)

        query = """INSERT INTO tiny_exchange.public.request_errors (ad_unit_id, date, request_body, error, request_ip)
                   VALUES ($1, NOW(), $2, $3, $4)"""
        args = (ad_unit_id, json_body, err, ip)
        try:
            await asyncio.shield(conn.execute(query, *args))
        except Exception as e:
            logging.error(str(e))

    @staticmethod
    async def store_request_response_info(conn: pg.Connection, ad_unit_id: int,
                                          request: Request, ip: Union[str, None]) -> Union[str, int, None]:
        """
        stores successful processed bid info - request-response, impressions and won bids
        """

        # info about site or app
        if 'site' in request.bid_request:
            site = json.dumps(request.bid_request['site'])
            app = None
        else:
            app = json.dumps(request.bid_request['app'])
            site = None

        # ----------- STORE REQUEST INFO ----------- #
        request_info_query = """INSERT INTO tiny_exchange.public.request_response (ad_unit_id,
        website, app, req_resp_id, date, no_bids, is_test, cur, request_ip, device_ip, device_type)
        VALUES ($1, $2, $3, $4, NOW(), $5, $6, $7, $8, $9, $10)"""
        if cur := request.bid_request.get('cur'):
            cur = json.dumps(cur)
        info_args = (ad_unit_id, site, app, request.bid_request['id'],
                     False if request.bid_response['seatbid'] else True,
                     True if request.bid_request['test'] else False,
                     cur, ip,
                     request.bid_request['device'].get('ip'),
                     request.bid_request['device'].get('devicetype', 0)
                     )

        request_id_query = f"""SELECT id FROM tiny_exchange.public.request_response
                              WHERE ad_unit_id = {ad_unit_id} AND req_resp_id = '{request.bid_request['id']}'"""
        try:
            await conn.execute(request_info_query, *info_args)
            if request_id := await conn.fetch(request_id_query):
                # if it is possible that there are 2 requests with equal ID and both pub and site/app ID are NULL,
                # I want to take the newest, which was inserted one row above
                request_id = request_id[-1].get('id')  # so, I will use -1 index
        except Exception as e:
            request_id = None
            logging.error("Request %s with NULL ID - %s", request.bid_request['id'], str(e))
        # ----------- STORE REQUEST INFO ----------- #

        # store impressions
        imps_to_store = list()
        for imp in request.bid_request['imp']:
            imp_to_store = list()  # it is a row in impressions table
            imp_to_store.append(request_id)
            imp_to_store.append(imp['id'])
            imp_to_store.append(imp.get('bidfloor', 0))

            # ad type
            ad_type = 'error'
            imp_keys = [k.lower() for k in imp.keys()]
            for k in ('video', 'audio', 'banner', 'native'):
                if k in imp_keys:
                    ad_type = k
            imp_to_store.append(ad_type)

            # ad size
            ad_size = 'n.g.'
            if ad_type in ('native', 'audio'):
                ad_size = '-'
            elif ad_type in ('banner', 'video'):
                w = imp.get('banner', imp.get('video')).get('w')
                h = imp.get('banner', imp.get('video')).get('h')
                if w and h:
                    ad_size = f"{w}x{h}"
            imp_to_store.append(ad_size)
            imps_to_store.append(tuple(imp_to_store))

        await Database.store_impressions(conn, imps_to_store)

        # store won bids, if they are
        if request.bid_response['seatbid']:
            bids_to_store = list()
            for seat in request.bid_response['seatbid']:
                for bid in seat['bid']:
                    bid_to_store = list()  # it is a row in bids table
                    bid_to_store.append(request_id)
                    bid_to_store.append(bid['id'])
                    bid_to_store.append(bid['impid'])
                    bid_to_store.append(seat['seat'])
                    bid_to_store.append(bid['price'])
                    bid_to_store.append(seat['ext']['provider_id'])
                    bids_to_store.append(tuple(bid_to_store))
            await Database.store_won_bids(conn, bids_to_store)
        return request_id  # return request ID for test purposes
