import os
import aioredis

from aiohttp import web


class Test:

    @staticmethod
    async def get_hit_count(request: web.Request) -> web.Response:
        if os.environ.get('IN_CONTAINER'):
            cache = await aioredis.create_redis_pool('redis://redis')
        else:
            cache = await aioredis.create_redis_pool('redis://127.0.0.1')
        v = await cache.incr('hits')
        cache.close()
        await cache.wait_closed()
        return web.Response(text=str(v))
