import asyncio
import os
import logging

from aiohttp import web
from ad_exchanger.RTB import RTB
from ..Forensic.utils import get_client_ip
from ad_exchanger.DB import Database
from asyncpg.connection import Connection
from copy import deepcopy
from uuid import uuid4
from redis import Redis


class BidRequestHandler:

    def __init__(self, conn: Connection, cache_ad_units: Redis, cache_providers: Redis):
        self.conn = conn
        self.ad_units = cache_ad_units
        self.ad_providers_cache = cache_providers

        # ad providers are stored in "program" memory,
        # 'cause there is no way to perform logical checks of is_active and is_test inside of Redis
        self.ad_providers = dict()  # ToDo - find a way to update these values
        for key in self.ad_providers_cache.keys('*'):
            self.ad_providers[key] = self.ad_providers_cache.hgetall(key)

    async def process_request(self, request: web.Request) -> web.Response:
        """
        1) send request to DSPs
        2) choose the win bids
        3) send response to SSP
        """

        # process request from SSP
        headers = request.headers
        ip = get_client_ip(headers)
        user_agent = headers.get('user-agent')
        # fetch ad unit endpoint hash
        if ad_unit_hash := request.match_info.get('hash'):
            # fetch ad unit info from cache

            if (ad_unit_info := self.ad_units.hgetall(ad_unit_hash)) and int(ad_unit_info[b'is_active']):
                ad_unit_info = {
                    'id': int(ad_unit_info[b'id']),
                    'ad_pub_id': int(ad_unit_info[b'ad_pub_id']),
                    'name': ad_unit_info[b'name'].decode(),
                    'is_test': bool(int(ad_unit_info[b'is_test'])),
                    'format_id': int(ad_unit_info[b'format_id']),
                    'javascript': ad_unit_info[b'javascript'].decode(),
                    'creation_date': ad_unit_info[b'creation_date'].decode()
                }
                ad_unit_id = ad_unit_info['id']
            else:
                return web.Response(reason='no such ad unit or ad unit is inactive', status=204)
        else:
            return web.Response(status=204)

        # check headers
        if 'x-openrtb-version' not in headers:
            asyncio.create_task(Database.log_request_err(self.conn, ad_unit_id,
                                                         'no "x-openrtb-version" field',
                                                         await request.json() if request.can_read_body else None,
                                                         ip))
            return web.Response(reason='no "x-openrtb-version" field', status=204)

        # check, if body exists and it is valid
        if request.can_read_body:
            body = await request.json()

            try:
                RTB.validate_bid_request(body)
            except Exception as e:  # I want to return nbr code 2 (invalid request) if validation falls
                asyncio.create_task(Database.log_request_err(self.conn, ad_unit_id, str(e), body, ip))
                logging.warning("Ad unit ID - %s - %s", ad_unit_id, str(e))
                return web.json_response({'id': body.get('id'), 'seatbid': [], 'nbr': 2})
        else:
            asyncio.create_task(Database.log_request_err(self.conn, ad_unit_id, 'Body missing', None, ip))
            return web.Response(reason='Body missing', status=204)

        request = RTB()
        request.dsp_side(body)

        # request to DSPs
        if os.environ.get('DEBUG') or request.bid_request['test'] or ad_unit_info['is_test']:  # test DSPs

            destinations = [{'id': k.decode(), 'endpoint': v[b'endpoint'].decode()}
                            for k, v in self.ad_providers.items()
                            if v[b'is_test'] and v[b'is_active']]

            responses = await request.send_dsp_requests(destinations, headers={'x-openrtb-version': '2.5',
                                                                               'content-type': headers.get('content-type')})
        else:  # real DSPs
            destinations = [{'id': k.decode(), 'endpoint': v[b'endpoint'].decode()}
                            for k, v in self.ad_providers.items()
                            if not v[b'is_test'] and v[b'is_active']]
            responses = await request.send_dsp_requests(destinations, headers={'x-openrtb-version': '2.5',
                                                                               'content-type': headers.get('content-type')})

        # ----------- assemble response to SSP -----------
        response_body = dict()
        response_body['id'] = body['id']
        response_body['cur'] = body.get('cur', 'USD')  # ToDo - fix the issue when responses can be with different currencies
        response_body['bidid'] = str(uuid4())
        response_body['seatbid'] = list()
        for resp in responses:
            if 'seatbid' in resp:
                response_body['seatbid'].extend(resp['seatbid'])
        request.ssp_side(response_body)

        # choosing decision model
        # 'at' is always there (Iron Maiden Egg) and 2 by default. IT CAN BE ONLY 1 or 2
        if request.bid_request['at'] == 1:
            request.first_price()
        else:
            request.second_price()

        # statistics to DB
        # ensure_future creates task in the running event loop. OR SOMETHING SIMILAR TO THIS
        # ToDo - find a way to remove deepcopy (?)
        asyncio.create_task(Database.store_request_response_info(self.conn, ad_unit_id, deepcopy(request), ip))

        for seat in request.bid_response['seatbid']:
            seat.pop('ext')

        return web.json_response(request.bid_response)
